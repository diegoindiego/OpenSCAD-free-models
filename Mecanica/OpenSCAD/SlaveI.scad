

//home/pablovc/Documentos/OpenSCAD-free-models/SistemaTiDi
include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Componentes/motores.scad>;
include<./../Utilidades/Utilities.scad>;

$fn=20;


module BaseSlave(medidaX=40,medidaY=40,medidaZ=40,medidaEstandar=40){

difference(){
resize([medidaX,medidaY,medidaZ])
sphere(r=medidaEstandar);
    //cubo abajo
    translate([-medidaEstandar,-medidaEstandar,-medidaEstandar*2])
    cube([medidaEstandar*2,2*medidaEstandar,medidaEstandar*2]);
    
     //cubo mitad
    translate([-medidaEstandar,-medidaEstandar*2,-medidaEstandar])
    cube([medidaEstandar*2,medidaEstandar*2,medidaEstandar*2]);
}

}




difference(){
    union(){
rotate(180)
BaseSlave(medidaX=40,medidaY=40,medidaZ=40/3);

BaseSlave(medidaX=40,medidaY=40*1.75,medidaZ=40/3);
    }
    
    scale(0.9)
    union(){
    
rotate(180)
BaseSlave(medidaX=40,medidaY=40,medidaZ=40/3);

BaseSlave(medidaX=40,medidaY=40*1.75,medidaZ=40/3);

        }
}
//hombro1
translate([-5,0,5])
rotate([0,0,90])
BaseSlave(medidaX=20,medidaY=20*0.75,medidaZ=20);

//hombro2
mirror([1,0,0]){
  translate([-5,0,5])
   rotate([0,0,90])
  BaseSlave(medidaX=20,medidaY=20*0.75,medidaZ=20);
}


//CABINA
translate([0,0,4])
union(){
rotate([0,0,180])
BaseSlave(medidaX=15,medidaY=30,medidaZ=30);

rotate([0,90,90])
    
    difference(){
        //PRIMER CILINDRO MODIFICADO
   resize([15,15,40])
    difference(){
    cylinder(r=40,h=40);
        translate([0,-40,-40])
    cube([80,80,80]);
        }
        //RECORTES EN ANGULO
        //primer recorte para puertas
        translate([2,0,13.5])
        rotate([0,20,0])
        translate([-8.5,-10,0])
        cube([20,20,20]);
        
         //primer recorte para puertas
        translate([3,0,18])
        rotate([0,-10,0])
        translate([-9.1,-10,0])
        cube([20,20,40]);
    }
}
/*


BaseSlave();
BaseSlave(ancho=40,factorX=1,factorZ=40/3,factor=1);

BaseSlave(ancho=40,factorX=1,factorZ=40/3,factor=1.75);

!rotate([0,0,90])
BaseSlave(ancho=40,factorX=1,factorZ=40,factor=1.75);
*/